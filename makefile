VERSION=0.2.0
LOVE_VERSION=11.3
NAME=Anchor
URL=https://gitlab.com/alexjgriffith/love-libs
AUTHOR="Alexander Griffith"
DESCRIPTION="A minimalist immediate mode UI implementation for love."

SRC := anchor.fnl
OUT := $(patsubst %.fnl,%.lua,$(SRC))

%.lua: %.fnl; lua ../fennel --compile --correlate $< > $@

build: $(OUT)
	mv anchor.lua init.lua

clean: ; rm -rf *.lua
