(local repl (require :vendor.stdio))
(local anchor (require :lib.anchor))

(var (mx my pressed) (values 0 0 false))

(var (cw ch) (love.window.getMode))

(var canvas (love.graphics.newCanvas cw ch))

(local titleFont (love.graphics.newFont 40))

(local buttonFont (love.graphics.newFont 30))

(fn callback [text]
  (fn []
    (pp (.. text " pressed!"))))

(fn button [text ox alpha callback]
  (anchor.push-element 0 0 300 40)
  (if (anchor.over mx my)
      (do (when pressed (callback))
          (love.graphics.setColor 1 1 0 alpha))
      (love.graphics.setColor 1 0 0 alpha))
  (love.graphics.rectangle :fill 0 0 300 40 5)
  (love.graphics.setFont buttonFont)
  (love.graphics.setColor 0 0 0 alpha)
  (love.graphics.printf text ox 0 300 :center)
  (anchor.pop))


(fn title [text]
  (anchor.push-element 0 0 300 70)
  (love.graphics.setFont titleFont)
  (love.graphics.printf text 0 0 300 :center)
  (anchor.pop))

(fn draw [canvas]
  (love.graphics.push :all)
  (love.graphics.setCanvas canvas)  
  (anchor.push-anchor 0 0 cw ch :top [0.5 0.25])
  (anchor.push-column 0 0 300 800 :top 15)
  (title "Hello World")
  (button "Button 1" 0 1 (callback "Button 1"))
  (button "Button 2" 0 1 (callback "Button 2"))
  (anchor.pop)
  (anchor.pop)
  (love.graphics.pop))

(fn love.load []
  (repl.start :vendor))

(fn love.draw []
  (love.graphics.draw canvas))

(fn love.update [dt]
  (set (mx my) (love.mouse.getPosition))
  (draw canvas)
  (set pressed false))

(fn love.mousepressed []
  (set pressed true))

(fn love.keypressed [key code]
  (love.event.quit))
