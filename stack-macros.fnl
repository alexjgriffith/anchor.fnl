;; MODULE: ANCHOR MACROS
;; LICENCE: LGPL3
;; AUTHOR: ALEXANDER GRIFFITH
;; DESCRIPTION: Stack tool for ANCHOR

;; This is done to allow us to use names to refer to stacks below, while
;; avoiding using string indexes in the inner render loop. There may
;; be a better way to do it in the future. Hopefully if this file is
;; improved, anchor.fnl won't have to be touched.

(local stacks {:stack-l 0 ; Quad Stack
               :stack-t 0
               :stack-w 0 
               :stack-h 0

               :stack-arrangement :anchor  ; Parameters
               :stack-alignment  :top-left
               :stack-anchor :top-left
               :stack-gutter-x 0
               :stack-gutter-y 0
               :stack-dx 0
               :stack-dy 0               
               
               :stack-window-t 0  ;;  viewport stack
               :stack-window-l 0
               :stack-window-w 0
               :stack-window-h 0
               :stack-viewport-t 0 ;; Screen Space
               :stack-viewport-l 0
               :stack-viewport-w 0
               :stack-viewport-h 0               
               })

(local max-stack-depth 20)

(fn max-stack [] max-stack-depth)

(fn rep [what times]
  (let [ret []]
    (for [i 1 times]
      (table.insert ret what))
    ret))

(fn create-stacks []
  (let [l (list 'local)
        a (list )
        b (list 'values)]
    (each [key default (pairs stacks)]
      (table.insert a  (sym key))
      (table.insert b (rep default max-stack-depth)))
    (table.insert l a)
    (table.insert l b)
    l))

(fn tset-stack-nil [stack i]
  (let [ out (list 'do)]
    (each [key default (pairs stacks)]
      (table.insert out `(tset ,(sym key) ,stack ,default)))
    out))

(fn last [array ?length]
  `(. ,array ,(or ?length (# array))))

(fn last-list [array ?length]
  (let [ret {}]
    (each [value _ (pairs array)]
      (tset ret value (last (sym value) ?length)))
    ret))

(fn stack-last []
  (last-list stacks))

(fn get-stack [stack-depth symbol ...]
  (let [ret (list 'values)]
    (table.insert ret `(. ,symbol ,stack-depth))
    (each [_ key (ipairs [...])]
      (table.insert ret `(. ,key ,stack-depth)))
    ret))

(fn set-stack [stack-depth symbol value ...]
  (let [ret (list 'do)
        vararg [...]]
    (table.insert ret `(tset ,symbol ,stack-depth ,value))
    (when (> (# vararg) 1)
      (for [i 1 (# vararg) 2]
        (table.insert ret
                      `(tset ,(. vararg i) ,stack-depth ,(. vararg (+ i 1))))))
    ret))

(fn get-quad [i]
  (get-stack (or i `stack-depth)
             `stack-l `stack-t
             `stack-w `stack-h))

(fn set-quad [l t w h]
  (set-stack `stack-depth
             `stack-l l
             `stack-t t
             `stack-w w
             `stack-h h))

(fn get-window [i]
  (get-stack (or i `window-depth)
             `stack-window-l `stack-window-t
             `stack-window-w `stack-window-h))

(fn set-window [l t w h]
  (set-stack `window-depth
             `stack-window-l l
             `stack-window-t t
             `stack-window-w w
             `stack-window-h h))

(fn get-viewport [i]
  (get-stack (or i `window-depth)
             `stack-viewport-l `stack-viewport-t
             `stack-viewport-w `stack-viewport-h))

(fn set-viewport [l t w h]
  (set-stack `window-depth
             `stack-viewport-l l
             `stack-viewport-t t
             `stack-viewport-w w
             `stack-viewport-h h))

(fn get-properties [i]
  (get-stack (or i `stack-depth)
             `stack-arrangement `stack-alignment `stack-anchor
             `stack-gutter-x `stack-gutter-y `stack-dx `stack-dy))

(fn set-gutter [x y]
  (set-stack `stack-depth
             `stack-gutter-x x
             `stack-gutter-y y))

(fn set-dxy [x y]
  (set-stack `stack-depth
             `stack-dx x
             `stack-dy y))

(fn set-arrangement [a]
  (set-stack `stack-depth
             `stack-arrangement a))

(fn get-arrangement [i]
  (get-stack (or i `stack-depth)
             `stack-arrangement))

(fn set-alignment [a]
  (set-stack `stack-depth
             `stack-alignment a))

(fn set-anchor [a]
  (set-stack `stack-depth
             `stack-anchor a))

{: create-stacks
 : get-quad
 : get-window
 : get-viewport
 : get-properties
 : set-quad
 : set-window
 : set-viewport
 : set-anchor
 : set-arrangement
 : get-arrangement 
 : set-alignment
 : set-dxy
 : set-gutter
 : get-stack
 : set-stack
 : max-stack
 : stack-last}
